import { Action } from '@ngrx/store';
import { PriceQueryResponse, PriceQuerySymbolResponse } from './price-query.type';

export enum PriceQueryActionTypes {
  SelectSymbol = 'priceQuery.selectSymbol',
  SymbolsRequested = 'priceQuery.symbolsRequested',
  SymbolsFetched = 'priceQuery.symbolsFetched',
  SymbolsFetchError = 'priceAuery.symbolsFetchError',
  FetchPriceQuery = 'priceQuery.fetch',
  PriceQueryFetched = 'priceQuery.fetched',
  PriceQueryFetchError = 'priceQuery.error'
}

export class FetchPriceQuery implements Action {
  readonly type = PriceQueryActionTypes.FetchPriceQuery;
  constructor(public symbol: string, public period: string) {}
}

export class PriceQueryFetchError implements Action {
  readonly type = PriceQueryActionTypes.PriceQueryFetchError;
  constructor(public error: any) {}
}

export class PriceQueryFetched implements Action {
  readonly type = PriceQueryActionTypes.PriceQueryFetched;
  constructor(public queryResults: PriceQueryResponse[]) {}
}

export class SelectSymbol implements Action {
  readonly type = PriceQueryActionTypes.SelectSymbol;
  constructor(public symbol: string) {}
}

export class SymbolsRequested implements Action {
  readonly type = PriceQueryActionTypes.SymbolsRequested;
}

export class SymbolsFetched implements Action {
  readonly type = PriceQueryActionTypes.SymbolsFetched;
  constructor(public symbolsResponse: PriceQuerySymbolResponse[]) {}
}

export class SymbolsFetchError implements Action {
  readonly type = PriceQueryActionTypes.SymbolsFetchError;
  constructor(public error: any) {}
}

export type PriceQueryAction =
  | FetchPriceQuery
  | PriceQueryFetched
  | PriceQueryFetchError
  | SelectSymbol
  | SymbolsRequested
  | SymbolsFetched
  | SymbolsFetchError
;

export const fromPriceQueryActions = {
  FetchPriceQuery,
  PriceQueryFetched,
  PriceQueryFetchError,
  SelectSymbol,
  SymbolsRequested,
  SymbolsFetched,
  SymbolsFetchError
};
