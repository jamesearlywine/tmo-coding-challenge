import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade, PriceQuerySymbolResponse } from '@coding-challenge/stocks/data-access-price-query';
import { Observable, Subject } from 'rxjs';
import { startWith, map, takeUntil, filter, withLatestFrom, take } from 'rxjs/operators';
import * as CustomValidators from '../validators/validators';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit, OnDestroy {
  private maxAutocompleteResults = 200;

  stockPickerForm: FormGroup;
  symbol: string;
  period: string;

  today: Date = new Date();
  fromMin: Date = null;
  fromMax: Date = new Date();
  toMin: Date = null;
  toMax: Date = new Date();



  quotes$ = this.priceQuery.priceQueries$;
  symbols$: Observable<PriceQuerySymbolResponse[]> = this.priceQuery.symbols$;
  filteredSymbolOptions$: Observable<PriceQuerySymbolResponse[]>

  destroy$ = new Subject<void>();

  timePeriods = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [
        {value: null, disabled: true},
        Validators.required,
        CustomValidators.existsInArrayItemPropertiesAsync(this.symbols$, ['symbol'])
      ],
      period: [null, Validators.required],
      periodFrom: [new Date(), Validators.required],
      periodTo: [new Date(), Validators.required],
    });
  }

  ngOnInit() {
    this.subscribeToSymbols();
    this.initFilteredSymbolOptions();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  subscribeToSymbols() {
    this.symbols$
      .pipe(take(1))
      .subscribe(symbols => {
        this.stockPickerForm.controls.symbol.enable();
      })
    ;
    this.priceQuery.fetchSymbols();
  }

  initFilteredSymbolOptions() {
    this.filteredSymbolOptions$ = this.stockPickerForm.controls.symbol.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        startWith (''),
        filter(value => value != null),
        withLatestFrom(this.symbols$),
        map(([value, symbols]) => this._filterSymbol(value, symbols))
      )
    ;
  }
  _filterSymbol(value, symbols) {
    const filterValue = value.toLowerCase();
    const results = symbols.filter( symbol => symbol.symbol.toLowerCase().includes(filterValue) );

    return results.slice(0,this.maxAutocompleteResults);
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, period } = this.stockPickerForm.value;
      this.priceQuery.fetchQuote(symbol, period);
    }
  }

  onDateChangeFrom(event) {
    const newDate = event.value;
    this.toMin = new Date(newDate.getTime());
  }
  onDateChangeTo(event) {
    const newDate = event.value;
    if (newDate < this.stockPickerForm.controls.periodFrom.value) {
      this.stockPickerForm.controls.periodFrom.setValue( new Date(newDate.getTime()) );
    }
    this.fromMax = new Date(newDate.getTime());
  }

}
