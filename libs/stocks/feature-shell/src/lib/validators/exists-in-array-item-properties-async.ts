import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { take, map } from 'rxjs/operators';

export function existsInArrayItemPropertiesAsync(array$: Observable<any[]>, properties: string[] = []): ValidatorFn {

  return (control: AbstractControl): Observable <ValidationErrors | null> => {
    return combineLatest(
      control.valueChanges,
      array$
    )
      .pipe(
        take (1),
        map( ([value, array]) => {
          const filterValue = value.toLowerCase();
          let existsInArray = false;

          for (const property of properties) {
            if (array
                  .filter(
                    arrValue => arrValue[property] && arrValue[property].toLowerCase().trim() === filterValue
                  )
                  .length > 0
            )
            {
              existsInArray = true;
            }
          }

          return existsInArray
            ? null
            : {
                existsInArrayItemPropertiesAsync: true
              } as ValidationErrors
          ;
        })
      )
    ;
  };

}
