import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig = {
  production: true,
  apiKey: 'pk_84cb5c5b45ab4912814224ed931945e0',
  apiURL: 'https://cloud.iexpais.com'
};
