'use strict'
import * as utils from './utils';
const CatboxMemory = require('@hapi/catbox-memory');

export const IEXProxyPlugin = {
  name: 'symbols-plugin',
  version: '0.0.1',
  once: true,
  register: async function(server, options) {

    /**
     * Cache Configuration
     */
    server.cache.provision({
      name: 'iex_cache',
      provider: {
        constructor: CatboxMemory
      }
    });

    const generalCache = server.cache({
      cache: 'iex_cache',
      expiresIn: 60 * 60 * 1000, // cache thing in general for one hour
      segment: 'general',
      generateFunc: async(fetchOptions) => {
        return await utils.fetch({...fetchOptions, verbose: false});
      },
      generateTimeout: 5000
    });

    const symbolsCache = server.cache({
      cache: 'iex_cache',
      expiresIn: 24 * 60 * 60 * 1000, // cache symbols for one day
      segment: 'symbols',
      generateFunc: async(fetchOptions) => {
        return await utils.fetch({...fetchOptions, verbose: true});
      },
      generateTimeout: 5000
    });

    const chartsCache = server.cache({
      cache: 'iex_cache',
      expiresIn: 20 * 60 * 1000, // cache charts data for 20 minutes
      segment: 'charts',
      generateFunc: async(fetchOptions) => {
        return await utils.fetch({...fetchOptions, verbose: false});
      },
      generateTimeout: 5000
    });


    /**
     * Routes
     */

     // quote-free route for testing caching
    server.route({
      method: 'GET',
      path: '/{version}/time-series',
      handler: async function(request, h) {
        const rawUrl = utils.getIexUrlFromRequest(request);
        const url = rawUrl.replace('{version}', request.params.version);
        const id = request.method + '::' + url;

        const value = await generalCache.get({
          id: id,
          method: request.method,
          url: url
        });

        return request.generateResponse(value);
      }
    });

    // route for symbols
    server.route({
      method: 'GET',
      path: '/{version}/ref-data/symbols',
      handler: async function(request, h) {
        const rawUrl = utils.getIexUrlFromRequest(request);
        const url = rawUrl.replace('{version}', request.params.version);
        const id = request.method + '::' + url;

        const value = await symbolsCache.get({
          id: id,
          method: request.method,
          url: url
        });

        return request.generateResponse(value);
      }
    });

    // route for chart data
    server.route({
      method: 'GET',
      path: '/{version}/stock/{symbol}/chart/{period}',
      handler: async function(request, h) {
        const rawUrl = utils.getIexUrlFromRequest(request);
        const url = rawUrl
          .replace('{version}', request.params.version)
          .replace('{symbol}', request.params.symbol)
          .replace('{period}', request.params.period)
        ;
        const id = request.method + '::' + url;

        const value = await chartsCache.get({
          id: id,
          method: request.method,
          url: url
        });

        return request.generateResponse(value);
      }
    });




  },

};
