'use strict'
import { environment } from '../environments/environment';
const Wreck = require('@hapi/wreck');

export function queryParamsToString(queryParams: any) {
  const queryStringFragments = [];

  Object.entries(queryParams)
    .forEach(([key, value]) => {
      const queryStringFragment = key + '=' + value;
      queryStringFragments.push(queryStringFragment);
    })
  ;

  const queryString = queryStringFragments.join('&');

  return '?' + queryString;

}

export function getIexUrlFromRequest(request) {
  const queryString = queryParamsToString(request.query);
  const fullPath = request.route.path + queryString;
  const url = environment.api.URL + fullPath;

  return url;
}

export const fetch = async (options) => {
  const {method, url, verbose} = options;
  const response = await Wreck.request(method, url, {timeout: 5000});
  const payload = await Wreck.read(response, {json: true, gunzip: true});

  if (verbose) {
    console.log('fetch called, options: ', options);
  }

  return payload;
};
