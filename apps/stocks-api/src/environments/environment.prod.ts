export const environment = {
  production: true,
  api: {
    URL: 'https://sandbox.iexapis.com',
    host: 'sandbox.iexapis.com',
    port: 80,
    protocol: 'http'
  }
};
