// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api: {
    URL: 'https://sandbox.iexapis.com',
    host: 'sandbox.iexapis.com',
    port: 80,
    protocol: 'http'
  }
};
